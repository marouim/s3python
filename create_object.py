from s3_client import s3
import sys
import json

object_name = "super-object"
bucket_name = "storage-app2-bucket1-ad8d1047-fc19-4dbc-a96f-c8560a7e110e"

object_to_write = {
  "name": "Un super objet",
  "value": "D'une valeur exceptionnelle",
  "meta": "Contenant beaucoup d'informations"
}

s3.put_object(
  Bucket=bucket_name,
  Key=object_name,
  Body=json.dumps(object_to_write)
)
